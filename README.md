# Flow Completion Time Measuring (FCTM) Tool
FCTM is a simple tool for sending a flow from one server to another and
measuring the Flow Completion Time (FCT).

## Compilation & Usage
1. Compile and create a binary called `fctm` using
`gcc fctm.c -O3 -o fctm` (on all machines that you want to use)
2. Start up a server (receiver) on one machine using `fctm -s DURATION`; the
server will listen for `DURATION` seconds (or, if a client (sender) still sends
data, the server (receiver) will run until that client has finished)
3. Start up one or multiple clients (senders) on a different machine using
`fctm -c FLOW_SIZE`; the client will send a flow with `FLOW_SIZE` packets.
The default IP address for the server (receiver) to connect to is
`13.37.3.0`. Use `-i SERVER_IP` to specify a different IP address. The client
will measure the FCT of the flow it sends and print it to stdout
